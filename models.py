import math
from dataclasses import dataclass
from typing import List, Tuple

import numpy as np
import sklearn.linear_model as linear_model
import sklearn.metrics as metrics
import sklearn.svm as svm


@dataclass
class ModelResult:
    model: object
    acc: float
    f_score: float
    conf_mat: np.ndarray


def convert_decision_to_probability(decision_np: np.ndarray) -> float:
    decision = float(decision_np[0])
    try:
        return 1 / (1 + (math.e ** (0 - decision)))
    except OverflowError:
        return float((math.copysign(1, decision) + 1) / 2)


def prep_data_for_training(train_data: List[Tuple[str, np.ndarray, bool]]) -> Tuple[List[np.ndarray], List[bool]]:
    x = []
    y = []
    for datum in train_data:
        x.append(datum[1])
        y.append(datum[2])
    return x, y


def evaluate_model(model, train_x: List[np.ndarray], train_y: List[bool]) -> (float, float, np.ndarray):
    pred_y = model.predict(train_x)
    acc = metrics.accuracy_score(train_y, pred_y)
    f_score = metrics.f1_score(train_y, pred_y)
    conf_mat = metrics.confusion_matrix(train_y, pred_y)
    return acc, f_score, conf_mat


def train_perceptron(train_data: List[Tuple[str, np.ndarray, bool]]) -> ModelResult:
    train_x, train_y = prep_data_for_training(train_data)
    model = linear_model.Perceptron(shuffle=False)
    print(f"Traning {model.__class__.__name__}")
    model.fit(train_x, train_y)
    return ModelResult(model, *evaluate_model(model, train_x, train_y))


def train_svm(train_data: List[Tuple[str, np.ndarray, bool]]) -> ModelResult:
    train_x, train_y = prep_data_for_training(train_data)
    model = svm.LinearSVC(random_state=0)
    print("Traning SVM")
    model.fit(train_x, train_y)
    return ModelResult(model, *evaluate_model(model, train_x, train_y))


def train_max_ent(train_data: List[Tuple[str, np.ndarray, bool]]) -> ModelResult:
    train_x, train_y = prep_data_for_training(train_data)
    model = linear_model.LogisticRegression(penalty='none')
    print("Traning MaxEnt")
    model.fit(train_x, train_y)
    return ModelResult(model, *evaluate_model(model, train_x, train_y))
