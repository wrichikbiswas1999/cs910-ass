import shutil
from multiprocessing import Pool
from pathlib import Path
from typing import Dict, List, Tuple, Union

import numpy as np

import utils

RESULTS_PATH = Path("AutoEq", "results")

MEDOID_CORRECTION = utils.Medoid.NONE


def get_all_targets_and_headphones() -> Tuple[Dict[str, List[Path]], Dict[str, List[Path]]]:
    in_ear_map = {}
    over_ear_map = {}
    for vendor in RESULTS_PATH.iterdir():
        if vendor.is_dir():
            for target in vendor.iterdir():
                if target.is_dir():
                    in_tgt = [hp for hp in target.iterdir() if target.is_dir()]
                    if "in-ear" in target.name:
                        if target.name not in in_ear_map:
                            in_ear_map[target.name] = []
                        in_ear_map[target.name] += in_tgt
                    else:
                        if target.name not in over_ear_map:
                            over_ear_map[target.name] = []
                        over_ear_map[target.name] += in_tgt
                    print(f"Processed {target}")
    in_ear_map = {k: sorted(v, key=lambda path: path.name) for k, v in in_ear_map.items()}
    over_ear_map = {k: sorted(v, key=lambda path: path.name) for k, v in over_ear_map.items()}
    return in_ear_map, over_ear_map


def recursive_map(f, it):
    return [recursive_map(f, x) if hasattr(x, '__iter__') else f(x) for x in it]


def detect_sim(tgt_1_list: List[Path], tgt_2_list: List[Path]) -> Union[None, Tuple[Path, Path]]:
    tgt_1_names = [item.name.lower() for item in tgt_1_list]
    tgt_2_names = [item.name.lower() for item in tgt_2_list]
    intersection = sorted(list(set(tgt_1_names) & set(tgt_2_names)))
    if len(intersection) == 0:
        return None
    else:
        return tgt_1_list[tgt_1_names.index(intersection[0])], tgt_2_list[tgt_2_names.index(intersection[0])]


def process_keys(headphones_map: Dict[str, List[Path]]) -> Tuple[np.ndarray,
                                                                 List[List[Union[Tuple[Path, Path],
                                                                                 None]]]]:
    print({tgt: len(headphones_map[tgt]) for tgt in headphones_map})
    targets = sorted(list(headphones_map.keys()), key=lambda key: headphones_map[key], reverse=True)
    print(targets)
    sim_list = np.zeros((len(targets), len(targets)), dtype=int)
    sim_tuples = [[None for _ in range(len(targets))] for _ in range(len(targets))]
    for i in range(len(targets)):
        for j in range(i, len(targets)):
            sim = detect_sim(headphones_map[targets[i]], headphones_map[targets[j]])
            if sim is not None:
                sim_list[i][j] = 1
                sim_tuples[i][j] = sim
                sim_tuples[j][i] = tuple(reversed(sim))
    return sim_list | np.transpose(sim_list), sim_tuples


def get_compensation(connections: List[List[Union[Tuple[Path, Path], None]]]) -> np.ndarray:
    comp_arr = np.zeros((len(connections), 127), dtype=float)
    for idx, conn in enumerate(connections[0]):
        dest = utils.get_auto_eq_measurements_from_path(conn[0])
        src = utils.get_auto_eq_measurements_from_path(conn[1])
        correction = dest - src
        comp_arr[idx] = correction
    return comp_arr


def write_corrections(headphones_map: Dict[str, List[Path]], enc_type: str, corrections_map: np.ndarray):
    targets = sorted(list(headphones_map.keys()), key=lambda key: headphones_map[key], reverse=True)
    dest_dir = utils.MEASUREMENTS_PATH / enc_type
    if not dest_dir.exists():
        dest_dir.mkdir(parents=True, exist_ok=True)
    for idx, target in enumerate(targets):
        for headphone in headphones_map[target]:
            dest_path = dest_dir / f"{headphone.name}.txt"
            if not dest_path.exists():
                hp_measurements = utils.get_auto_eq_measurements_from_path(headphone)
                corrected = hp_measurements + corrections_map[idx]
                if MEDOID_CORRECTION == utils.Medoid.MEDIAN:
                    corrected -= np.median(corrected)
                elif MEDOID_CORRECTION == utils.Medoid.MEAN:
                    corrected -= np.mean(corrected)
                with dest_path.open(mode='w') as dest_file:
                    dest_file.write(str(list(corrected)))
                print(f"Written {target} to {dest_path}")


def main():
    in_ear_map, over_ear_map = get_all_targets_and_headphones()
    in_ear_flags, in_ear_conns = process_keys(in_ear_map)
    in_ear_corrections = get_compensation(in_ear_conns)
    over_ear_flags, over_ear_conns = process_keys(over_ear_map)
    over_ear_corrections = get_compensation(over_ear_conns)
    if utils.MEASUREMENTS_PATH.exists():
        shutil.rmtree(utils.MEASUREMENTS_PATH)
    with Pool() as pool:
        in_future = pool.apply_async(write_corrections, (in_ear_map, "in_ear", in_ear_corrections))
        over_future = pool.apply_async(write_corrections, (over_ear_map, "over_ear", over_ear_corrections))
        in_future.get()
        over_future.get()


if __name__ == '__main__':
    main()
