import random
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter, Namespace
from csv import DictWriter
from pathlib import Path
from typing import List, Tuple, Dict

import numpy as np
import scipy.special

import models
import utils


def get_arguments() -> Namespace:
    arg_parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    arg_parser.add_argument('-t', '--type', default=utils.EnclosureType.OVER_EAR.value,
                            choices=[enc.value for enc in utils.EnclosureType],
                            type=str.lower,
                            help="Type of headphone (case-insensitive)")
    arg_parser.add_argument('-p', '--positive', type=Path, default=Path("positive.txt"),
                            help='File with list of liked headphones, one per line')
    arg_parser.add_argument('-n', '--negative', type=Path, default=Path("negative.txt"),
                            help='File with list of disliked headphones, one per line')
    arg_parser.add_argument('-o', '--output', type=Path, default=Path("out.csv"),
                            help='File with output of recommendations')
    args = arg_parser.parse_args()
    return args


def get_headphones_to_process_shuffled(positive_file: Path,
                                       negative_file: Path,
                                       enc_type: utils.EnclosureType) -> List[Tuple[str, np.ndarray, bool]]:
    if not utils.MEASUREMENTS_PATH.exists():
        print("Mesurements Directory not found. Please run make_corrections.py.")
        exit(1)
    headphones_list = []
    for idx, file in enumerate([positive_file, negative_file]):
        count = len(headphones_list)
        print(f"Processing {file}")
        with file.open() as file_in:
            for name in file_in.read().splitlines():
                y = bool(1 - idx)
                try:
                    measurements = utils.get_corrected_measurements(name, enc_type)
                    headphones_list.append((name, measurements, y))
                    print(f"Read {name} as {'dis' if not y else ''}liked")
                except FileNotFoundError as e:
                    print(f"Failed to read {name}: {e}")
        if len(headphones_list) == count:
            print(f"No inputs from {file}, may cause problematic predictions")
    random.shuffle(headphones_list)
    return headphones_list


def get_as_dict(decisions: np.ndarray,
                probabilities: np.ndarray,
                means: np.ndarray,
                hp_names: List[str],
                index: int) -> Dict[str, object]:
    return {
        "Name": hp_names[index],
        "Perceptron Score": float(decisions[index][0]),
        "Perceptron Probability": float(probabilities[index][0]),
        "SVM Score": float(decisions[index][1]),
        "SVM Probability": float(probabilities[index][1]),
        "MaxEnt Score": float(decisions[index][2]),
        "MaxEnt Probability": float(probabilities[index][2]),
        "Mean Probability": float(means[index])
    }


def write_outputs(decisions: np.ndarray,
                  probabilities: np.ndarray,
                  means: np.ndarray,
                  hp_names: List[str],
                  output_file: Path):
    indices = sorted(range(len(hp_names)), key=lambda idx: means[idx], reverse=True)
    with output_file.open(mode='w') as csv_file:
        csv_writer = DictWriter(csv_file, fieldnames=["Name",
                                                      "Perceptron Score",
                                                      "Perceptron Probability",
                                                      "SVM Score",
                                                      "SVM Probability",
                                                      "MaxEnt Score",
                                                      "MaxEnt Probability",
                                                      "Mean Probability"])
        csv_writer.writeheader()
        for index in indices:
            csv_writer.writerow(get_as_dict(decisions, probabilities, means, hp_names, index))


def main():
    args = get_arguments()
    enc_type = utils.EnclosureType(args.type)
    print(f"Predicting with {enc_type.value.replace('_', '-')} headphones")
    positive_file = args.positive.resolve()
    print(f"File with liked headphones: {positive_file}")
    negative_file = args.negative.resolve()
    print(f"File with disliked headphones: {negative_file}")
    output_file = args.output.resolve()
    print(f"File for output: {output_file}")
    random.seed(0)
    train_data = get_headphones_to_process_shuffled(positive_file, negative_file, enc_type)
    if len(train_data) == 0:
        print("Nothing to predict")
        exit(1)
    perc = models.train_perceptron(train_data)
    print(perc)
    svm = models.train_svm(train_data)
    print(svm)
    max_ent = models.train_max_ent(train_data)
    print(max_ent)
    all_names, all_data = utils.get_all_for_enclosure(enc_type)
    print(all_data.shape)
    print(np.zeros(shape=(all_data.shape[0], 3), dtype=float).shape)
    decisions = np.zeros(shape=(all_data.shape[0], 3), dtype=float)
    decisions[:, 0] = perc.model.decision_function(all_data)
    decisions[:, 1] = svm.model.decision_function(all_data)
    decisions[:, 2] = max_ent.model.decision_function(all_data)
    probabilities = scipy.special.expit(decisions)
    means = np.mean(probabilities, 1)
    print("Writing outputs")
    write_outputs(decisions, probabilities, means, all_names, output_file)
    print("Written outputs")


if __name__ == '__main__':
    main()
