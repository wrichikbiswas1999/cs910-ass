import shutil
import subprocess
from enum import Enum, auto, unique
from functools import cache, lru_cache
from multiprocessing import Pool
from pathlib import Path
from typing import List, Tuple

import numpy as np

MEASUREMENTS_PATH = Path("measurements")


@unique
class Medoid(Enum):
    NONE = auto()
    MEDIAN = auto()
    MEAN = auto()


@unique
class EnclosureType(Enum):
    OVER_EAR = "over_ear"
    IN_EAR = "in_ear"


@cache
def get_auto_eq_commit() -> str:
    git_cmd = shutil.which("git")
    proc = subprocess.run([git_cmd, "submodule"], capture_output=True)
    splits = [line.split(" ") for line in proc.stdout.decode().splitlines()]
    for line in splits:
        if line[2] == 'AutoEq':
            return line[1]


@lru_cache
def get_auto_eq_measurements_from_path(path: Path) -> np.ndarray:
    hp_file = path / f"{path.name} GraphicEQ.txt"
    with hp_file.open() as file_in:
        measurements = file_in.readline()
    return np.asarray([float(measurement.split(" ")[1]) for measurement in measurements[11:].split("; ")])


@lru_cache
def get_corrected_measurements(name: str, enc_type: EnclosureType) -> np.ndarray:
    hp_file = MEASUREMENTS_PATH / enc_type.value / f"{name}.txt"
    with hp_file.open() as file_in:
        measurements = file_in.readline()
    loaded = measurements
    return np.asarray([float(level) for level in loaded[1:-1].split(", ")], dtype=float)


def get_all_for_enclosure(enc_type: EnclosureType) -> Tuple[List[str], np.ndarray]:
    hp_dir = MEASUREMENTS_PATH / enc_type.value
    hp_list = [item.name[:-4] for item in hp_dir.iterdir()]
    with Pool() as pool:
        futures = [pool.apply_async(get_corrected_measurements, (hp, enc_type)) for hp in hp_list]
        np_measures = [future.get() for future in futures]
    return hp_list, np.asarray(np_measures)
