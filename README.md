# Using ML to recommend headphones

This repository contains code to produce ML models using the user's FR preferences and produce a CSV file with outputs
of recommendations. It uses the AutoEq results to generate the models. The models' decision scores are then activated
using the sigmoid function and then averaged to generate the ranks.

## Requirements

In order to use this repository, you need `Python 3.9`

## Usage

1. Clone this repository
2. Update the AutoEq repository by running: ```git submodule update --remote --init```
3. Install requirements like: ```python3.9 -m pip install -r requirements.txt```
4. Run the corrections script: ```python3.9 make_corrections.py```
5. Add prediction parameters
    1. Create two files with liked and disliked headphones
        1. One name per line
        2. The names must match the names of the files generated by the corrections script
        3. The files can contain both in-ear and over-ear preferences
6. Run the predictions script: ```python3.9 make_predictions.py```
    1. Run ```python3.9 make_predictions.py -h```
